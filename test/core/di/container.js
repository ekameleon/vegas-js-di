
import chai from 'chai'

import container from '../../../src/index'

const { assert } = chai ;

describe( 'core.di.container', () =>
{
    describe( 'Use a basic parameter definition', () =>
    {
        after( () =>
        {
            container.clear();
        } )

        before( () =>
        {
            container.add( 'bar', 'bar', ['tag1'] );
        } )

        it( "container.has('foo') === foo", () =>
        {
            assert.isFalse( container.has( 'foo' ) );
        } );

        it( "container.has('bar') === true", () =>
        {
            assert.isTrue( container.has( 'bar' ) );
        } );

        it( "container.get('bar') === 'bar'", () =>
        {
            assert.equal( container.get( 'bar' ), 'bar' );
        } );

        it( "container.remove('bar')", () =>
        {
            container.remove( 'bar' );
            assert.isFalse( container.has( 'bar' ) );
        } );
    } )
} )
