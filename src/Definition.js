export const FACTORY   = 'factory';
export const PARAMETER = 'parameter';
export const SERVICE   = 'service';

/**
 * The {core.di.Definition|Definition} is a basic definition entry in the container cache.
 * @name Definition
 * @class
 * @memberof core.di
 */
class Definition
{
    constructor( type , value )
    {
        this.type  = type;
        this.value = value ;
    }

    isFactory = () => this.type === FACTORY ;

    isParameter = () => this.type === PARAMETER ;

    isService = () => this.type === SERVICE ;

    toString = () => '[' + this.constructor.name + ']' ;
}

export default Definition ;