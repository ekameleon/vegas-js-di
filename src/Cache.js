import notEmpty from 'vegas-js-core/src/strings/notEmpty'

/**
 * The {core.di.Cache|Cache} interface is the internal map to register the object definitions.
 * @name Cache
 * @class
 * @memberof core.di
 */
class Cache
{
    constructor()
    {
        this._objects = {};
    }

    add = ( name , value ) =>
    {
        if( notEmpty( name ) )
        {
            this._objects[name] = value ;
        }
        else
        {
            throw new TypeError( this + ' add() failed, the name parameter must be a valid string.' ) ;
        }
    }

    clear = () =>
    {
        this._objects = {};
    }

    get = name =>
    {
        if( notEmpty( name ) )
        {
            if ( this.has( name ) )
            {
                return this._objects[name];
            }
            else
            {
                throw new ReferenceError( this + ' get() failed, the name is not found.');
            }
        }
        else
        {
            throw new TypeError( this + ' get() failed, the name parameter must be a valid string.' ) ;
        }
    };

    has = name => name in this._objects ;

    remove = name =>
    {
        if( notEmpty( name ) )
        {
            if ( this.has( name ) )
            {
                delete this._objects[name] ;
            }
            else
            {
                throw new ReferenceError( this + ' remove() failed, the name argument is not found.' ) ;
            }
        }
        else
        {
            throw new TypeError( this + ' remove() failed, the name parameter must be a valid string.' ) ;
        }
    }

    toString = () => '[' + this.constructor.name + ']' ;
}

export default Cache ;