import Cache from './Cache'

import isString from 'vegas-js-core/src/isString'
import notEmpty from 'vegas-js-core/src/strings/notEmpty'

/**
 * The {core.di.Tags|Tags} is a tag cache map.
 * @name Tags
 * @class
 * @memberof core.di
 * @extends core.di.Cache
 */
class Tags extends Cache
{
    constructor()
    {
        super() ;
    }

    add = ( name, tags ) =>
    {
        if( notEmpty( name ) )
        {
            if( tags instanceof Array )
            {
                tags.forEach( ( tag, index ) =>
                {
                    if ( !isString( tag ) )
                    {
                        throw new TypeError( 'Tags add failed, the tag defined at index ' + index + ' must be a string value.');
                    }
                });
                this._objects[name] = tags ;
            }
            else
            {
               throw new TypeError( 'Tags add failed, the tags parameter must be an Array.' ) ;
            }
        }
        else
        {
            throw new TypeError( 'Tags add failed, the name parameter must be a valid string.' ) ;
        }
    }

    getByTag = tag =>
    {
        let result = [] ;
        for ( let name in this._objects )
        {
            if ( this._objects.hasOwnProperty(name) )
            {
                let search = this._objects[name].some( item => item === tag ) ;
                if ( search )
                {
                    result = [ ...result , name ] ;
                }
            }
        }
        return result;
    }
}

export default Tags ;