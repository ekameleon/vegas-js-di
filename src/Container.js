import isString from 'vegas-js-core/src/isString'
import notEmpty from 'vegas-js-core/src/strings/notEmpty'

import Definition from './Definition'

import {
    FACTORY ,
    PARAMETER ,
    SERVICE
}
from './Definition';

/**
 * A simple dependency injection container.
 * @name Container
 * @class
 * @memberOf core.di
 * @example
 *
 * class Log
 * {
 *     constructor( message )
 *     {
 *         this.message = message;
 *     }
 * }
 *
 * let count = 0 ;
 *
 * container.add( 'bar'     , 'bar' , [ 'tag1' ] ) ;
 * container.add( 'foo'     , container.parameter( 'Foo' ) ) ;
 * container.add( 'test'    , container.parameter( message => new Log( message + ' :: ' + count++ ) ) ) ;
 * container.add( 'logger'  , container.factory( ( container , message ) => new Log( message ) ) ) ;
 * container.add( 'console' , container => new Log( container.get( 'test' )( 'Hola Mundo' ).message ) , [ 'tag1' , 'tag2' ]) ;
 *
 * console.log( container.has( 'foo'     ) ) ;
 * console.log( container.has( 'logger'  ) ) ;
 * console.log( container.has( 'unknown' ) ) ;
 *
 * const test1 = container.get( 'test' ) ;
 * console.log( test1( 'Bonjour Monde').message ) ;
 *
 * const test2 = container.get( 'test' ) ;
 * console.log( test2( 'Bonjour World').message ) ;
 *
 * const log1 = container.get( 'logger', 'hello world' ) ;
 * console.log( log1.message ) ;
 *
 * const log2 = container.get( 'console' ) ;
 * console.log( log2.message ) ;
 *
 * console.log( container.tagged('tag1') ) ;
 */
class Container
{
    /**
     * Creates a new Container instance.
     * @constructor
     */
    constructor( cache , definitions , tags )
    {
        this._cache       = cache;
        this._definitions = definitions ;
        this._tags        = tags;
    }

    /**
     * Add a dependency definition to the container that can later be retrieved by name.
     * The dependency can be a value, object or a factory function that returns an object.
     * @param name {string} The identifier of the entry to look for.
     * @param value {*} A factory function or a parameter to register in the container.
     * @param {string|string[]} tags The tag or the optional list of tags to tag a reference in the container.
     * @throws TypeError if the identifier is not a valid string value.
     * @throws TypeError if a tag is not a valid string value.
     */
    add = ( name , value , tags = null ) =>
    {
        if( notEmpty( name ) )
        {
            if( notEmpty( tags ) )
            {
                tags = [ tags ] ;
            }

            if ( tags instanceof Array )
            {
                if ( tags.length > 0 )
                {
                    tags.forEach( ( tag , index ) =>
                    {
                        if ( !isString( tag ) )
                        {
                            throw new TypeError( this + ' add() failed, the tag defined at index ' + index + ' must be string value.');
                        }
                    });
                }
            }

            if ( !( tags instanceof Array ) )
            {
                tags = [] ;
            }

            let definition ;

            if ( value instanceof Function )
            {
                definition = new Definition( SERVICE , value );
            }
            else if ( value instanceof Definition )
            {
                definition = value ;
            }
            else
            {
                definition = new Definition( PARAMETER , value ) ;
            }

            this._definitions.add( name , definition );
            this._tags.add( name , tags );
        }
        else
        {
            throw new TypeError( this + ' add() failed, the name parameter must be a valid string.' );
        }
    }

    /**
     * Clear the container.
     */
    clear = () =>
    {
        this._cache.clear() ;
        this._tags.clear() ;
    }

    /**
     * Generates a function as a factory invoked in the container to build and returning a cached result.
     * @param {function} func The function invoked by the factory to build the entry in the container.
     * @returns {Definition} The definition reference to map in the container.
     * @throws TypeError if the passed-in parameter is not a valid function reference.
     */
    factory = func =>
    {
        if ( func instanceof Function )
        {
            return new Definition( FACTORY , func ) ;
        }
        else
        {
            throw new TypeError( this + ' factory() failed, the passed-in parameter must be a function.') ;
        }
    };

    /**
     * Finds an entry of the container by its identifier and returns it.
     * @param {string } name The identifier of the entry to look for.
     * @param {...*} args - The arguments passed-in the factory reference.
     * @returns {*} The entry reference in the container.
     * @throws ReferenceError if the identifier is not registered in the container.
     * @throws ReferenceError if the internal factory generate a null or undefined object.
     * @throws TypeError if the identifier is not a valid string value.
     */
    get = ( name , ...args ) =>
    {
        if( notEmpty( name ) )
        {
            if( this._definitions.has( name ) )
            {
                const definition = this._definitions.get( name );
                if ( definition.isFactory() )
                {
                    const { value } = definition ;
                    if ( args.length > 0 )
                    {
                        return value( this , ...args );
                    }
                    return value( this ) ;
                }
                else if ( definition.isService() )
                {
                    if( this._cache.has( name ) )
                    {
                        return this._cache.get( name ) ;
                    }

                    const factory = definition.value ;
                    const object  = factory( this );

                    if( object === null || object === undefined )
                    {
                        throw new ReferenceError( this + ' get() failed, the factory \'' + name + '\' does not return a value.' );
                    }

                    this._cache.add( name , object ) ;

                    return object ;
                }
                else
                {
                    return definition.value ;
                }
            }
            else
            {
                throw new ReferenceError( this + ' get() failed, the requested dependency "' + name + '" is not found.');
            }
        }
        else
        {
            throw new TypeError( this + ' get() failed, the name identifier must be a valid string.' );
        }
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     * @param {string} name The identifier of the entry to look for.
     * @returns {boolean} Returns true if the container can return an entry for the given identifier, otherwise false.
     */
    has = name => this._definitions.has( name ) ;

    /**
     * Inject a function and protects it from being invoked by the container so that the function itself can be returned.
     * @param value {*} An object of function to register in the container has parameter.
     * @returns {Definition} The definition reference of the passed-in value.
     */
    parameter = value => new Definition( PARAMETER , value ) ;

    /**
     * Remove a dependency definition in the container.
     * @param name {string} The identifier of the entry to look for.
     * @throws ReferenceError if the identifier is not found.
     * @throws TypeError if the identifier is not a valid string value.
     */
    remove = name =>
    {
        if( notEmpty( name ) )
        {
            if( this._definitions.has( name ) )
            {
                this._definitions.remove( name );
                if ( this._cache.has( name ) )
                {
                    this._cache.remove( name );
                }
                this._tags.remove( name );
            }
            else
            {
                throw new ReferenceError( this + ' remove failed, the requested dependency "' + name + '" is not found.');
            }
        }
        else
        {
            throw new TypeError( this + ' remove failed, the name parameter must be a valid string.' );
        }
    }

    /**
     * Generate a definition with a function and identifies it as a service locator
     * @param {Function} func The function to register in the container.
     * @returns {Definition} The new definition to inject in the container.
     */
    service = func =>
    {
        if ( func instanceof Function )
        {
            return new Definition( SERVICE , func ) ;
        }
        else
        {
            throw new TypeError( this + ' service() failed, the value must be a function.' ) ;
        }
    };

    /**
     * Will return the names of factory definitions tagged with given tag.
     * @param {string} tag - The name of the tag to found.
     * @returns {Array} The collection of identifiers tagged with the given tag.
     */
    tagged = tag =>
    {
        if ( isString( tag ) )
        {
            return this._tags.getByTag( tag );
        }
        else
        {
            throw new TypeError( this + ' tagged() failed, the tag must be a string value.' ) ;
        }
    }

    toString = () => '[' + this.constructor.name + ']' ;
}

export default Container ;