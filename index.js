import Container from './src/Container'
import Cache     from './src/Cache'
import Tags      from './src/Tags'

/**
 * The {@link core.di} library is a light-weight dependency injection container library.
 * @summary The {@link core.di} library is light-weight, strongly-typed messaging tools.
 * @license {@link https://www.mozilla.org/en-US/MPL/2.0/|MPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/gpl-2.0.fr.html|GPL 2.0} / {@link https://www.gnu.org/licenses/old-licenses/lgpl-2.1.fr.html|LGPL 2.1}
 * @author Marc Alcaraz <ekameleon@gmail.com>
 * @namespace core.di
 * @version 1.0.0
 * @since 1.0.0
 * @example
 * class Log
 * {
 *     constructor( message )
 *     {
 *         this.message = message;
 *     }
 * }
 *
 * let count = 0 ;
 *
 * container.add( 'bar'     , 'bar' , [ 'tag1' ] ) ;
 * container.add( 'foo'     , container.parameter( 'Foo' ) ) ;
 * container.add( 'test'    , container.parameter( message => new Log( message + ' :: ' + count++ ) ) ) ;
 * container.add( 'logger'  , container.factory( ( container , message ) => new Log( message ) ) ) ;
 * container.add( 'console' , container => new Log( container.get( 'test' )( 'Hola Mundo' ).message ) , [ 'tag1' , 'tag2' ]) ;
 *
 * console.log( container.has( 'foo'     ) ) ;
 * console.log( container.has( 'logger'  ) ) ;
 * console.log( container.has( 'unknown' ) ) ;
 *
 * const test1 = container.get( 'test' ) ;
 * console.log( test1( 'Bonjour Monde').message ) ;
 *
 * const test2 = container.get( 'test' ) ;
 * console.log( test2( 'Bonjour World').message ) ;
 *
 * const log1 = container.get( 'logger', 'hello world' ) ;
 * console.log( log1.message ) ;
 *
 * const log2 = container.get( 'console' ) ;
 * console.log( log2.message ) ;
 *
 * console.log( container.tagged('tag1') ) ;
 */
const container = new Container( new Cache() , new Cache() , new Tags() );

export default container ;