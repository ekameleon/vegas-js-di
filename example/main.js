import container from '../src/index'

class Log
{
    constructor( message )
    {
        this.message = message;
    }
}

let count = 0 ;

container.add( 'bar'     , 'bar' , [ 'tag1' ] ) ;
container.add( 'foo'     , container.parameter( 'Foo' ) ) ;
container.add( 'test'    , container.parameter( message => new Log( message + ' :: ' + count++ ) ) ) ;
container.add( 'logger'  , container.factory( ( container , message ) => new Log( message ) ) ) ;
container.add( 'console' , container => new Log( container.get( 'test' )( 'Hola Mundo' ).message ) , [ 'tag1' , 'tag2' ]) ;

console.log( container.has( 'foo'     ) ) ;
console.log( container.has( 'logger'  ) ) ;
console.log( container.has( 'unknown' ) ) ;

const test1 = container.get( 'test' ) ;
console.log( test1( 'Bonjour Monde').message ) ;

const test2 = container.get( 'test' ) ;
console.log( test2( 'Bonjour World').message ) ;

const log1 = container.get( 'logger', 'hello world' ) ;
console.log( log1.message ) ;

const log2 = container.get( 'console' ) ;
console.log( log2.message ) ;

console.log( container.tagged('tag1') ) ;

console.log( '----------' ) ;

class GeoCoordinates
{
    constructor( latitude , longitude )
    {
        this.latitude  = latitude ;
        this.longitude = longitude ;
    }

    toString = () => '{' + this.latitude + ',' + this.longitude + '}' ;
}

class Place
{
    constructor( name , geo )
    {
        this.name = name ;
        this.geo  = geo ;
    }

    toString = () => '[' + this.constructor.name + ']' ;
}

container.add( 'config' ,
{
    name : 'Béhuard' ,
    geo  :
    {
        latitude  : 47.3797 ,
        longitude : -0.6431
    }
})

container.add( 'geo' , container =>
{
    const { geo : { latitude, longitude } = {} } = container.get( 'config' ) ;
    return new GeoCoordinates( latitude , longitude ) ;
}) ;

container.add( 'place' , container =>
{
    const config = container.get( 'config' ) ;
    return new Place
    (
        config.name ,
        container.get( 'geo' )
    ) ;
}) ;


const place = container.get( 'place' ) ;

console.log( '> ' + place.name + ' ::: ' + place.geo ) ;