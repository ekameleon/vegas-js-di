# Vegas JS DI - openSource library - Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.4] - 2024-10-09
### Changed
* Upgrade the vegas-js-core dependency, target the version 1.0.43

## [1.0.3] - 2021-04-28
### Changed
* Upgrade the vegas-js-core dependency, target the version 1.0.28

## [1.0.0] - 2021-04-23
### Added
* Basic singleton to use the container implementation
* First version of the library

