# VEGAS JS

**Vegas JS | DI** - **version 1.0.5** is an *Opensource* Library based on **ECMAScript** for apply the Dependency Injection pattern with a fast container implementation.

### About

 * Author : Marc ALCARAZ (aka eKameleon) - Creative Technologist and Digital Architect
 * Mail : ekameleon[at]gmail.com
 * LinkedIn : [https://www.linkedin.com/in/ekameleon/](https://www.linkedin.com/in/ekameleon/)

### License

Under tree opensource licenses :

 * [License MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/)
 * [License GPL 2.0+](https://www.gnu.org/licenses/gpl-2.0.html)
 * [License LGPL 2.1+](https://www.gnu.org/licenses/lgpl-2.1.html)

## Resources

#### ⌜ Download

Download on **Bitbucket** the latest code, report an issue, ask a question or contribute :

 * [https://bitbucket.org/ekameleon/vegas-js-signals](https://bitbucket.org/ekameleon/vegas-js-di)

#### ⌜ Slack Community

![slack-logo-vector-download.jpg](https://bitbucket.org/repo/AEbB9b/images/3509366499-slack-logo-vector-download.jpg)

Send us your email to join the **VEGAS** community on Slack !

## Install

#### ⌜ YARN / NPM

```console
yarn add vegas-js-di
```

or

```console
npm install vegas-js-di
```

## Usage


#### ⌜ Unit tests

We use the [Mocha](https://mochajs.org) and the Chai (http://chaijs.com/) tools to run the unit tests of the VEGAS JS libraries.

1 - Run all unit tests
```
$ yarn test
```

2 - Run a specific library, use one of this command :

The **--match** option trigger the unit test engine (based on **[Mocha](https://mochajs.org/)**) to only run tests matching the given pattern which is internally compiled to a RegExp, for examples :

```
$ yarn test -g system.signals.Receiver
```

The **--reporter** option define the unit test result rendering in the terminal with the values : 'spec', 'dot', 'landing', 'dot', 'nyan', 'list', 'mochawesome'. By default the 'spec' value is used.

```console
yarn test --reporter nyan
```
![vegas-js-signals-nyan.png](https://bitbucket.org/repo/7ErBBx6/images/979441029-vegas-js-signals-nyan.png)

## History

 * 1998 : Flash
 * 2000 : First framework concept and first libraries (components, tools, design patterns)
 * 2004 : First official SVN repository
 * 2007 : Fusion with the Maashaack framework (eden, etc.)
 * 2015 : Google Code must die - **VEGAS** move from an old Google Code SVN repository to this Bitbucket GIT repository and REBOOT this source code.
 * 2016 : Begin the new JS architecture of the VEGAS JS library based on ES6
 * 2018 : Cut the JS packages of VEGAS in a set of independent libraries.
 